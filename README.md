This role installs splunk forwarder and simply sets it as deployment client

### Requirements ###

* Install Ansible, e.g. for RHEL-based: sudo yum install ansible
* SSH key-based auth should be set up with target host

### How do I get set up? ###

* Edit splunk-forwarder.yml and provide your vars
* execute playbook splunk-forwarder.yml: ansible-playbook -i <target host>, splunk-forwarder.yml